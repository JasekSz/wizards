var lastLoopRun = 0,
    enemies = new Array(),
    iterations = 0,
    time = 45,
    counter = 0,
    screenWidth = $(window).width()/3.5,
    j = 250,
    ammo = q/8,
    fail = ['Moja babcia lepiej by to zrobiĹa...', 'SĹabiutko...', 'Zacznij klikaÄ tÄ myszkÄ :/'],
    win = ['CaĹkiem, caĹkiem...', 'NieĹźle!', 'Sam bym tego lepiej nie zrobiĹ :)'];


window.setInterval(countDown, 1000);

// tworzenie wrogĂłw

createAmmo();

function createTarget(element, x, y, w, h, killed, annoying) {

    var result = new Object();


    result.element = element;
    if (annoying) {
        result.x = y+screenWidth;
        result.y = x-screenWidth;
    } else {
        result.x = x;
        result.y = y;
    }
    result.w = w;
    result.h = h;
    result.killed = killed;
    result.annoying = annoying;
    return result;
}

// ********************* Animation of annoying big bug ******************

function wander() {
    rotationX += 360;
    let $board = $('#board');
    var x = (($board.width() - 80) / 2) * (Math.random() * 1.3 - 0.9) + screenWidth/1.5,
        y = (($board.height() - 200) / 2) * (Math.random() * 5.4 - 0.7);
    TweenLite.to(bigBug, 2.5, {x:x, y:y, ease:Power1.easeInOut, rotation:rotationX, onComplete:wander});
}
wander();

// ************* Big Bug shooted **************


$("#bigBug").click(function (e) {
    if (ammo > 0) {
        var audio = new Audio('sounds/femaledeath6.wav');
        audio.play();
        counter -= 5;
        $('#score').html(counter);
        createInfo('-5', e);
    }
});


// sprawdzanie czy nie wychodzÄ wrogowie poza planszÄ

function ensureBounds(target, ignoreY) {
    if (target.x < 40) {
        target.x = 40;
    }
    if (!ignoreY && target.y < 40) {
        target.y = 40;
    }
    if (target.x + target.w > 530+screenWidth+50) {
        target.x = 530+screenWidth+50 - target.w;
    }
    if (!ignoreY && target.y + target.h > 530+screenWidth) {
        target.y = 530+screenWidth - target.h;
    }
}

function setPosition(target) {
    var e = document.getElementById(target.element);
    e.style.left = target.x + 'px';
    e.style.top = target.y + 'px';
}

// sprawdzanie czy wrogowie nie uciekli

function checkCollision() {
    for (var i = 0; i < enemies.length; i++) {
        if (enemies[i].y + enemies[i].h >= 580 && enemies[i].killed === false) {
            var element = document.getElementById(enemies[i].element);
            element.style.visibility = 'hidden';
            element.parentNode.removeChild(element);
            enemies.splice(i, 1);
            i--;
            killReaction(false, 1, false, false);
        } else if (enemies[i].x + enemies[i].w >= screenWidth+550 && enemies[i].killed === false && enemies[i].annoying === true) {
            var element = document.getElementById(enemies[i].element);
            element.style.visibility = 'hidden';
            element.parentNode.removeChild(element);
            enemies.splice(i, 1);
            i--;
        }
    }
}

function showTargets() {
    for (var i = 0; i < enemies.length; i++) {
        setPosition(enemies[i]);
    }
}

// funkcja do poruszania wrogami

function updatePositions() {
    for (var i = 0; i < enemies.length; i++) {
        if (enemies[i].annoying === false) {
            enemies[i].y += 4;
            enemies[i].x += getRandom(7) - 3;
            ensureBounds(enemies[i], true);
        } else {
            enemies[i].x += 4;
            enemies[i].y += getRandom(7) - 3;
            ensureBounds(enemies[i], true);
        }
    }
}

// funcja do tworzenia wrogĂłw

function addTarget(img, size, value, quantity, annoying) {
    var interval = quantity;
    if (iterations > 1500) {
        interval = quantity / 10;
    } else if (iterations > 1000) {
        interval = quantity / 5;
    } else if (iterations > 500) {
        interval = quantity / 2;
    }

    if (getRandom(interval) == 0) {
        var elementName = 'enemy' + getRandom(10000000);
        var enemy = createTarget(elementName, getRandom(550)+screenWidth, -20, size, size, false, annoying);

        var element = document.createElement('div');
        element.id = enemy.element;
        element.className = 'enemy';
        document.children[0].appendChild(element);
        $(element).css("background-image", "url(img/" + img);
        $(element).click(function (e) {
            if(ammo > 0) {
                killReaction(e, value, 'maledeath', annoying);
                $(this).hide("fast");
                enemy.killed = true;
            }
        });

        enemies[enemies.length] = enemy;
    }
}

// pÄtla ktĂłra wykonuje cyklicznie funcje

function loop() {
    if(time != 0) {
        if (new Date().getTime() - lastLoopRun > 40) {
            updatePositions();
            checkCollision();

            addTarget('strawberry.png', 35, 1, q / 2, false);
            addTarget('blueberry.png', 25, 2, q / 1.5, false);
            addTarget('radish.png', 25, 3, q, false);
            addTarget('plum.png', 25, 4, q * 2, false);
            addTarget('3.gif', 25, -4, q / 2, true);

            showTargets();

            lastLoopRun = new Date().getTime();
            iterations++;
        }
        setTimeout('loop();', 2);
    }else{
        return;
    }
}

// reakcja na zestrzelenie bÄdĹş dotniÄcie krawÄdzi ekranu

function killReaction(e, value, sound, annoying) {
    if (e && sound) {
        counter += value;
        $('#score').html(counter);
        u = getRandom(6) + 1;
        if (annoying == false) {
            createInfo('+' + value, e);
            var audio = new Audio('sounds/' + sound + u + '.wav');
            audio.play();
        } else {
            createInfo(value, e);
            var audio = new Audio('sounds/femaledeath'+ u + '.wav');
            audio.play();
        }
    } else {
        counter -= value;
        $('#score').html(counter);
        createInfo('-' + value);
        var u = getRandom(7) + 1;
        var audio = new Audio("sounds/angry"+ u +".wav");
        audio.play();
    }
};

loop();

// ********************** info creating ***************
function createInfo(info, e) {
    var divName = 'info' + getRandom(100);
    var newDiv = document.createElement("divName");
    TweenLite.set(newDiv, {color:"hsl(" + Math.random() * 255 + ", 90%, 60%)"});
    if(e) {
        var xPosition = e.clientX;
        var yPosition = e.clientY;
        document.body.appendChild(newDiv);
        newDiv.className = "info";
        $(newDiv).html(info)
            .css('left', xPosition)
            .css('top', yPosition)
            .css('display', 'block')
            .animate({
                opacity: 0,
                left: screenWidth,
                top: "5"
            }, 2000);
    } else {
        document.body.appendChild(newDiv);
        newDiv.className = "info";
        $(newDiv).html(info)
            .css('display', 'block')
            .animate({
                opacity: 0,
                left: screenWidth,
                top: "5"
            }, 2000);
    }
}


// ******************** Counter ************************


function update(element, content, klass) {
    var p = element.firstChild || document.createElement("p");
    p.textContent = content;
    element.appendChild(p);
    if (klass) {
        p.className = klass;
    }
}

function countDown() {
    time--;
    update($timer, time);
    if (time === 0) {
        gameOver();
    }
}

// ************************ Game over *********************

function gameOver() {
    $('#restart').css('display', 'block');
    let i = getRandom(3);
    if (counter < 20) {
        var audio = new Audio('sounds/fail.mp3');
        audio.play();
        $('#board').html('<h1>Koniec gry!<br>'+fail[i]+'<br>Liczba punktĂłw: ' + counter + '</h1><br><img src="img/deadbug.png" height="200" width="190"><a href ="index.html"><img src="img/again.png" id="restart" height="50" width="150" ></a>');
    } else {
        var audio = new Audio('sounds/victory.wav');
        audio.play();
        $('#board').html('<h1>Koniec gry!<br>'+win[i]+'<br>Liczba punktĂłw: ' + counter + '</h1><br><img src="img/coolbug.png" height="200" width="190"><a href ="index.html"><img src="img/again.png" id="restart" height="50" width="150" ></a>');
    }
    pulse($('#restart'));
    spinX($('#restart'));
    $(window).unbind("click");
    $('.enemy').remove();
    setTimeout(function(){$('.enemy').remove()}, 300);
}








